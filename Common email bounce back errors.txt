Q: Common email bounce back errors

Ans:
1  Mailbox not found
2 Invalid mailbox
3 User unknown
4 Mailbox unavailable
5 Mailbox not found

6 Mailbox full
7 Mail quota exceeded

These error messages are showing that the recipient's mailbox is full and the server will not accept any more messages until the user make more space available.

8  Connection timed out
9   Resources temporarily unavailable

 DNS configuration error with the recipient domain's MX records. This can be addressed by the owner of the recipient address.

10  Could not complete sender verify